﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml.Linq;
using WorkWithXML;

namespace UI
{
    public partial class FormContacts : Form
    {
        public FormContacts()
        {
            InitializeComponent();
        }

        private int id = 0;
        private void Form1_Load(object sender, EventArgs e)
        {
            UpdateListView();
            listView_Contacts.FullRowSelect = true;      
        }

        private void btn_Delete_Click(object sender, EventArgs e)
        {
            WorkXML xml = new WorkXML();
            xml.DeleteItem(id);
            ClearTextbox();
            CheckGrouping();
        }

        public void UpdateListView()
        {
            listView_Contacts.Items.Clear();
            WorkXML xml = new WorkXML();
            List<string[]> items = new List<string[]>();
            items = xml.ReadXMLOutputTable();
            foreach (string[] item in items)
            {
                listView_Contacts.Items.Add(new ListViewItem(item));
            }
            items.Clear(); 
        }

        private TextBox Resolve(int number)
        {
            switch (number)
            {
                case 0:
                    return txt_Firstname;                   
                case 1:
                    return txt_Lastname;            
                case 2:
                    return txt_Group;
                case 3:
                    return txt_Home_number;
                case 4:
                    return txt_Mobile_number;
            }
            return null;
        }

        private void listView_Contacts_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listView_Contacts.SelectedIndices.Count == 0) return;
            var index = listView_Contacts.SelectedIndices[0];
            for (int i = 0; i < listView_Contacts.Items[index].SubItems.Count; i++)
            {
                if (i == 5)
                    id = Convert.ToInt32(listView_Contacts.Items[index].SubItems[i].Text);
                else
                {
                    Resolve(i).Text = listView_Contacts.Items[index].SubItems[i].Text;
                }
                                
            }
        }

        private void btn_Save_Click(object sender, EventArgs e)
        {
            WorkXML xml = new WorkXML();
            xml.ChangeItem(id,txt_Firstname.Text,txt_Lastname.Text,txt_Group.Text,txt_Home_number.Text,txt_Mobile_number.Text);
            CheckGrouping();
            ClearTextbox();
        }

        private void btn_Add_Click(object sender, EventArgs e)
        {
            WorkXML xml = new WorkXML();
            bool check = xml.AddItem(txt_Firstname.Text,txt_Lastname.Text,txt_Group.Text,txt_Home_number.Text,txt_Mobile_number.Text);
            CheckGrouping();
            if (check) MessageBox.Show("You do not enter a name or surname");
            ClearTextbox();
            
        }

        private void ClearTextbox()
        {
            txt_Firstname.Clear();
            txt_Lastname.Clear();
            txt_Group.Clear();
            txt_Home_number.Clear();
            txt_Mobile_number.Clear();
        }
        private void txtFilter_TextChanged(object sender, EventArgs e)
        {
            List<string[]> items = new List<string[]>();
            WorkXML xml = new WorkXML();
            IEnumerable<XElement> operation = xml.SearchElements(txtFilter.Text);
            if (operation != null)
            {
                items = xml.OutputFoundElements(operation);
                listView_Contacts.Items.Clear();
                foreach (string[] item in items)
                {
                    listView_Contacts.Items.Add(new ListViewItem(item));
                    if (chbGrouping.Checked)
                    {
                        for (int i = 0; i < listView_Contacts.Items.Count; i++)
                        {
                            GroupItem(listView_Contacts.Items[i]);
                        }
                    }
                } 
            }
            else CheckGrouping();              
        }

        private void GroupItem(ListViewItem item)
        {
            bool group_exists = false;
            foreach (ListViewGroup group in this.listView_Contacts.Groups)
            {
                if (group.Header == "Group "+item.SubItems[2].Text)
                {

                    item.Group = group;
                    group_exists = true;
                    break;
                }
            }
            if (!group_exists)
            {
                ListViewGroup group = new ListViewGroup("Group "+item.SubItems[2].Text);
                this.listView_Contacts.Groups.Add(group);
                item.Group = group;
            }
        }

        private void chbGrouping_CheckedChanged(object sender, EventArgs e)
        {
            CheckGrouping();
        }

        private void CheckGrouping()
        {
            if (chbGrouping.Checked)
            {
                UpdateListView();
                for (int i = 0; i < listView_Contacts.Items.Count; i++)
                {
                    GroupItem(listView_Contacts.Items[i]);
                }
            }
            else UpdateListView();
        }
    }
}
