﻿namespace UI
{
    partial class FormContacts
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.ListViewGroup listViewGroup2 = new System.Windows.Forms.ListViewGroup("Group", System.Windows.Forms.HorizontalAlignment.Left);
            this.listView_Contacts = new System.Windows.Forms.ListView();
            this.columnFirstname = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnLastname = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnGroup = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHomeNumber = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnMobileNumber = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.txt_Firstname = new System.Windows.Forms.TextBox();
            this.txt_Lastname = new System.Windows.Forms.TextBox();
            this.txt_Home_number = new System.Windows.Forms.TextBox();
            this.txt_Mobile_number = new System.Windows.Forms.TextBox();
            this.lbl_Firstname = new System.Windows.Forms.Label();
            this.lbl_Lastname = new System.Windows.Forms.Label();
            this.lbl_Home_number = new System.Windows.Forms.Label();
            this.lbl_Mobile_number = new System.Windows.Forms.Label();
            this.btn_Save = new System.Windows.Forms.Button();
            this.btn_Add = new System.Windows.Forms.Button();
            this.btn_Delete = new System.Windows.Forms.Button();
            this.lbl_Group = new System.Windows.Forms.Label();
            this.txt_Group = new System.Windows.Forms.TextBox();
            this.txtFilter = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.chbGrouping = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // listView_Contacts
            // 
            this.listView_Contacts.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnFirstname,
            this.columnLastname,
            this.columnGroup,
            this.columnHomeNumber,
            this.columnMobileNumber});
            listViewGroup2.Header = "Group";
            listViewGroup2.Name = "Group";
            this.listView_Contacts.Groups.AddRange(new System.Windows.Forms.ListViewGroup[] {
            listViewGroup2});
            this.listView_Contacts.Location = new System.Drawing.Point(23, 65);
            this.listView_Contacts.Name = "listView_Contacts";
            this.listView_Contacts.Size = new System.Drawing.Size(368, 213);
            this.listView_Contacts.TabIndex = 0;
            this.listView_Contacts.UseCompatibleStateImageBehavior = false;
            this.listView_Contacts.View = System.Windows.Forms.View.Details;
            this.listView_Contacts.SelectedIndexChanged += new System.EventHandler(this.listView_Contacts_SelectedIndexChanged);
            // 
            // columnFirstname
            // 
            this.columnFirstname.Text = "Firstname";
            // 
            // columnLastname
            // 
            this.columnLastname.Text = "Lastname";
            // 
            // columnGroup
            // 
            this.columnGroup.Text = "Group";
            // 
            // columnHomeNumber
            // 
            this.columnHomeNumber.Text = "HomeNumber";
            // 
            // columnMobileNumber
            // 
            this.columnMobileNumber.Text = "MobileNumber";
            // 
            // txt_Firstname
            // 
            this.txt_Firstname.Location = new System.Drawing.Point(497, 65);
            this.txt_Firstname.Name = "txt_Firstname";
            this.txt_Firstname.Size = new System.Drawing.Size(100, 20);
            this.txt_Firstname.TabIndex = 1;
            // 
            // txt_Lastname
            // 
            this.txt_Lastname.Location = new System.Drawing.Point(497, 112);
            this.txt_Lastname.Name = "txt_Lastname";
            this.txt_Lastname.Size = new System.Drawing.Size(100, 20);
            this.txt_Lastname.TabIndex = 2;
            // 
            // txt_Home_number
            // 
            this.txt_Home_number.Location = new System.Drawing.Point(497, 186);
            this.txt_Home_number.Name = "txt_Home_number";
            this.txt_Home_number.Size = new System.Drawing.Size(100, 20);
            this.txt_Home_number.TabIndex = 3;
            // 
            // txt_Mobile_number
            // 
            this.txt_Mobile_number.Location = new System.Drawing.Point(497, 235);
            this.txt_Mobile_number.Name = "txt_Mobile_number";
            this.txt_Mobile_number.Size = new System.Drawing.Size(100, 20);
            this.txt_Mobile_number.TabIndex = 4;
            // 
            // lbl_Firstname
            // 
            this.lbl_Firstname.AutoSize = true;
            this.lbl_Firstname.Location = new System.Drawing.Point(420, 72);
            this.lbl_Firstname.Name = "lbl_Firstname";
            this.lbl_Firstname.Size = new System.Drawing.Size(52, 13);
            this.lbl_Firstname.TabIndex = 5;
            this.lbl_Firstname.Text = "Firstname";
            // 
            // lbl_Lastname
            // 
            this.lbl_Lastname.AutoSize = true;
            this.lbl_Lastname.Location = new System.Drawing.Point(420, 119);
            this.lbl_Lastname.Name = "lbl_Lastname";
            this.lbl_Lastname.Size = new System.Drawing.Size(53, 13);
            this.lbl_Lastname.TabIndex = 6;
            this.lbl_Lastname.Text = "Lastname";
            // 
            // lbl_Home_number
            // 
            this.lbl_Home_number.AutoSize = true;
            this.lbl_Home_number.Location = new System.Drawing.Point(420, 193);
            this.lbl_Home_number.Name = "lbl_Home_number";
            this.lbl_Home_number.Size = new System.Drawing.Size(73, 13);
            this.lbl_Home_number.TabIndex = 7;
            this.lbl_Home_number.Text = "Home number";
            // 
            // lbl_Mobile_number
            // 
            this.lbl_Mobile_number.AutoSize = true;
            this.lbl_Mobile_number.Location = new System.Drawing.Point(420, 238);
            this.lbl_Mobile_number.Name = "lbl_Mobile_number";
            this.lbl_Mobile_number.Size = new System.Drawing.Size(76, 13);
            this.lbl_Mobile_number.TabIndex = 8;
            this.lbl_Mobile_number.Text = "Mobile number";
            // 
            // btn_Save
            // 
            this.btn_Save.Location = new System.Drawing.Point(522, 295);
            this.btn_Save.Name = "btn_Save";
            this.btn_Save.Size = new System.Drawing.Size(75, 23);
            this.btn_Save.TabIndex = 9;
            this.btn_Save.Text = "Save";
            this.btn_Save.UseVisualStyleBackColor = true;
            this.btn_Save.Click += new System.EventHandler(this.btn_Save_Click);
            // 
            // btn_Add
            // 
            this.btn_Add.Location = new System.Drawing.Point(425, 295);
            this.btn_Add.Name = "btn_Add";
            this.btn_Add.Size = new System.Drawing.Size(75, 23);
            this.btn_Add.TabIndex = 10;
            this.btn_Add.Text = "Add";
            this.btn_Add.UseVisualStyleBackColor = true;
            this.btn_Add.Click += new System.EventHandler(this.btn_Add_Click);
            // 
            // btn_Delete
            // 
            this.btn_Delete.Location = new System.Drawing.Point(110, 309);
            this.btn_Delete.Name = "btn_Delete";
            this.btn_Delete.Size = new System.Drawing.Size(75, 23);
            this.btn_Delete.TabIndex = 11;
            this.btn_Delete.Text = "Delete";
            this.btn_Delete.UseVisualStyleBackColor = true;
            this.btn_Delete.Click += new System.EventHandler(this.btn_Delete_Click);
            // 
            // lbl_Group
            // 
            this.lbl_Group.AutoSize = true;
            this.lbl_Group.Location = new System.Drawing.Point(422, 156);
            this.lbl_Group.Name = "lbl_Group";
            this.lbl_Group.Size = new System.Drawing.Size(36, 13);
            this.lbl_Group.TabIndex = 12;
            this.lbl_Group.Text = "Group";
            // 
            // txt_Group
            // 
            this.txt_Group.Location = new System.Drawing.Point(497, 153);
            this.txt_Group.Name = "txt_Group";
            this.txt_Group.Size = new System.Drawing.Size(100, 20);
            this.txt_Group.TabIndex = 13;
            // 
            // txtFilter
            // 
            this.txtFilter.Location = new System.Drawing.Point(373, 15);
            this.txtFilter.Name = "txtFilter";
            this.txtFilter.Size = new System.Drawing.Size(100, 20);
            this.txtFilter.TabIndex = 14;
            this.txtFilter.TextChanged += new System.EventHandler(this.txtFilter_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(323, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 13);
            this.label1.TabIndex = 15;
            this.label1.Text = "Filter";
            // 
            // chbGrouping
            // 
            this.chbGrouping.AutoSize = true;
            this.chbGrouping.Location = new System.Drawing.Point(70, 18);
            this.chbGrouping.Name = "chbGrouping";
            this.chbGrouping.Size = new System.Drawing.Size(69, 17);
            this.chbGrouping.TabIndex = 16;
            this.chbGrouping.Text = "Grouping";
            this.chbGrouping.UseVisualStyleBackColor = true;
            this.chbGrouping.CheckedChanged += new System.EventHandler(this.chbGrouping_CheckedChanged);
            // 
            // FormContacts
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(618, 364);
            this.Controls.Add(this.chbGrouping);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtFilter);
            this.Controls.Add(this.txt_Group);
            this.Controls.Add(this.lbl_Group);
            this.Controls.Add(this.btn_Delete);
            this.Controls.Add(this.btn_Add);
            this.Controls.Add(this.btn_Save);
            this.Controls.Add(this.lbl_Mobile_number);
            this.Controls.Add(this.lbl_Home_number);
            this.Controls.Add(this.lbl_Lastname);
            this.Controls.Add(this.lbl_Firstname);
            this.Controls.Add(this.txt_Mobile_number);
            this.Controls.Add(this.txt_Home_number);
            this.Controls.Add(this.txt_Lastname);
            this.Controls.Add(this.txt_Firstname);
            this.Controls.Add(this.listView_Contacts);
            this.Name = "FormContacts";
            this.Text = "Contacts";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView listView_Contacts;
        private System.Windows.Forms.TextBox txt_Firstname;
        private System.Windows.Forms.TextBox txt_Lastname;
        private System.Windows.Forms.TextBox txt_Home_number;
        private System.Windows.Forms.TextBox txt_Mobile_number;
        private System.Windows.Forms.Label lbl_Firstname;
        private System.Windows.Forms.Label lbl_Lastname;
        private System.Windows.Forms.Label lbl_Home_number;
        private System.Windows.Forms.Label lbl_Mobile_number;
        private System.Windows.Forms.Button btn_Save;
        private System.Windows.Forms.Button btn_Add;
        private System.Windows.Forms.Button btn_Delete;
        private System.Windows.Forms.ColumnHeader columnFirstname;
        private System.Windows.Forms.ColumnHeader columnLastname;
        private System.Windows.Forms.ColumnHeader columnGroup;
        private System.Windows.Forms.ColumnHeader columnHomeNumber;
        private System.Windows.Forms.Label lbl_Group;
        private System.Windows.Forms.TextBox txt_Group;
        private System.Windows.Forms.ColumnHeader columnMobileNumber;
        private System.Windows.Forms.TextBox txtFilter;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox chbGrouping;

    }
}

