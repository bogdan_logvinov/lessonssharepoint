﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Xml.Linq;
using System.Text.RegularExpressions;

namespace WorkWithXML
{
    public class WorkXML
    {
        public XDocument xDoc = XDocument.Load("contacts.xml");
        private string strIdContact = "id_contact";
        private string strFirstname = "firstname";
        private string strLastname = "lastname";
        private string strGroup = "group";
        private string strHomeNumber = "home_number";
        private string strMobileNumber = "mobile_number";

        public List<string[]> ReadXMLOutputTable()
        {
            List<string[]> rows = new List<string[]>();

                foreach (XElement elem in xDoc.Descendants("contact"))
                {
                    string[] newRow = new string[6];

                    if (elem.Element(strFirstname) != null)
                    {
                        newRow[0] = elem.Element(strFirstname).Value;
                    }

                    if (elem.Element(strLastname) != null)
                    {
                        newRow[1] = elem.Element(strLastname).Value;
                    }

                    if (elem.Element(strGroup) != null)
                    {
                        newRow[2] = elem.Element(strGroup).Value;
                    }

                    if (elem.Element(strHomeNumber) != null)
                    {
                        newRow[3] = elem.Element(strHomeNumber).Value;
                    }

                    if (elem.Element(strMobileNumber) != null)
                    {
                        newRow[4] = elem.Element(strMobileNumber).Value;
                    }
                    if (elem.Element(strIdContact) != null)
                    {
                        newRow[5] = elem.Element(strIdContact).Value;
                    }

                    rows.Add(newRow);
                }
            return rows;
        }

        public void DeleteItem(int id)
        {
            IEnumerable<XElement> operation = xDoc.Root.Element("table_contacts").Descendants("contact")
                .Where(t => t.Element(strIdContact).Value == id.ToString()).ToList();

                foreach (XElement t in operation)
                {
                    t.Remove();
                    break;
                }
                xDoc.Save("contacts.xml");
        }

        public void ChangeItem(int id, string firstName, string lastName, string group, string homeNumber,string mobileNumber)
        {

            

            IEnumerable<XElement> operation = xDoc.Root.Element("table_contacts").Descendants("contact")
                .Where(t => t.Element(strIdContact).Value == id.ToString()).ToList();
            foreach (XElement t in operation)
            {
                
                t.Element(strFirstname).SetValue(firstName);
                t.Element(strLastname).SetValue(lastName);
                t.Element(strGroup).SetValue(group);
                t.Element(strHomeNumber).SetValue(homeNumber);
                t.Element(strMobileNumber).SetValue(mobileNumber);
                xDoc.Save("contacts.xml");
            }
        }

        public bool AddItem(string firstname,string lastname,string group,string homeNumber,string mobileNumber)
        {            
            XDocument xDoc = XDocument.Load("contacts.xml");
            int maxId = xDoc.Root.Element("table_contacts").Descendants("contact").Max(t => Convert.ToInt32(t.Element(strIdContact).Value));
                
            if (firstname != "" && lastname != "" && homeNumber !="" && mobileNumber!="")
            {
                XElement contact = new XElement("contact",
                        new XElement(strIdContact, maxId++),
                        new XElement(strFirstname, firstname),
                        new XElement(strLastname, lastname),
                        new XElement(strGroup, group),
                        new XElement(strHomeNumber, homeNumber),
                        new XElement(strMobileNumber, mobileNumber));
                xDoc.Root.Element("table_contacts").Add(contact);
                xDoc.Save("contacts.xml");
                return false;
            }
            else return true;
        }

        public IEnumerable<XElement> SearchElements(string filterText)
        {
            XDocument xDoc = XDocument.Load("contacts.xml");
            IEnumerable<XElement> operation = null;
            if (filterText != "")
            {
                operation =
                    xDoc.Root.Element("table_contacts")
                        .Descendants("contact")
                        .Where(
                            t =>
                                Regex.IsMatch(t.Element(strFirstname).Value, "^" + filterText + "+$*") ||
                                Regex.IsMatch(t.Element(strLastname).Value, "^" + filterText + "+$*"))
                        .ToList();
            }
            return operation;
        }

        public List<string[]> OutputFoundElements(IEnumerable<XElement> operation)
        {
            List<string[]> foundingRows = new List<string[]>();
            foreach (XElement t in operation)
            {
                string[] newRow = new string[5];

                if (t.Element(strFirstname) != null)
                {
                    newRow[0] = t.Element(strFirstname).Value;
                }

                if (t.Element(strLastname) != null)
                {
                    newRow[1] = t.Element(strLastname).Value;
                }

                if (t.Element(strGroup) != null)
                {
                    newRow[2] = t.Element(strGroup).Value;
                }

                if (t.Element(strHomeNumber) != null)
                {
                    newRow[3] = t.Element(strHomeNumber).Value;
                }

                if (t.Element(strMobileNumber) != null)
                {
                    newRow[4] = t.Element(strMobileNumber).Value;
                }
                foundingRows.Add(newRow);
            }
            return foundingRows;
        }
    }
}
